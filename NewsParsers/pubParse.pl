#!/usr/bin/perl -w -l
use strict;
use LWP::Simple;

# need to implement an exo config file
my $days = 1;
my $max_results = 500;

open my $jrnl_handle, "<", "journals.txt" or die "Failed to open list of journals";
open my $ml_handle, ">>", "masterlist.csv" or die "Failed to create masterlist.csv";

#open my $dbg_handle, ">", "source.txt" or die "tombstone";

my @jrnl_list = <$jrnl_handle>;
chomp @jrnl_list;

my @pmid_list;

# get the pmids of papers published from joi for the last few days
foreach (@jrnl_list) {
	s/ /+/g;
	$_.='[journal]';
	my $url_pmid = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=$_&reldate=$days&datetype=edat&retmax=$max_results";
	my $source_pmid = get($url_pmid) or die "Could not connect to EUtils";
	
	while ($source_pmid =~ /<Id>(.+?)<\/Id>/g) {
		push @pmid_list, $1;
	}
}

# batch fetch
my $search_string = join(',',@pmid_list);
my $url_fetch = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=$search_string";

my $source_fetch = get($url_fetch) or die "Could not get batch";
$source_fetch =~ s/\n//g;

# so it turns out you dont actually need to enclose .+? in (), only when yall need to cap em
while ($source_fetch =~ /Pubmed-entry ::= .+?title.+?"(.+?)".+?jta "(.+?)".+?year ([0-9]+),.+?month ([0-9]+),.+?day ([0-9]+).+?doi "(.+?)".+?abstract "(.+?)",/gs) {
	
	my $title = $1; #ok!
	my $journal = $2;
	my $doi = $6;
	my $abstract = $7;	
	my $link = "http://www.ncbi.nlm.nih.gov/pubmed/$6";
	my $date = join('-',$3,$4,$5);

	s/,//g foreach ($title,$abstract);
	print $ml_handle my $entry = join(',',my @entry = ($journal,$date,$title,$doi,$link,$abstract));
}

__END__
[TO DO LIST]
-> die statements
-> add a readme