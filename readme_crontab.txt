to start the crontab daemon, go to your terminal 
and type the following, without the '>':

>crontab snipt_cron.txt

to edit the crontab daemon, either
	1) enter >crontab -e
		i) a text editor will appear
		ii) make changes as needed, save and exit
	2) edit snipt_cron.txt as needed

	for more information on the crontab format, please go to
	
>en.wikipedia.com/wiki/Cron

to remove any crontab daemons, go to your terminal
and type the following:

>crontab -r
