#!/usr/bin/perl -l
use warnings;
use strict;
use DBI;
require "./michael.lib";

#Declare database information
my $dbName = "";
my $hostAddress = "";
my $port = "";
my $host = "DBI:mysql:database=$dbName;host=;port=$port";
my $account = "";
my $password = "";
# pA_id, pB_id, url, date_published, source_name, annotated, int_type_id, cell_type_name


# Open the database 
my $dbh =DBI->connect($host, $account,$password)
or die("Problem connecting: ", DBI->errstr);

my $query = "show tables"; ##CUSTOM QUERY HERE##


#MORE SAMPLE USAGE (not tested yet)
#my $query = "insert into protein (p_id, p_fullname, p_sym, p_of_interest)values(?, ?, ?, ?)";
#my ($sth, $run) = run_query($dbh, $query, [$entrez_id, $full_name, $sym, 0], 1)

my ($sth, $run) = run_query($dbh, $query, []);

print @$_ foreach dbToArray($sth, $run);

#if ($run!=0) {
#    #code
#    while (my @row = $sth->fetchrow_array) {
#        print "@row";
#    }
#    print "num of affected rows:", $run;
#}


print "PA url PB - expected: exist(1) actual: ", isDupPPI($dbh, '6657', '79832', 'http://www.ncbi.nlm.nih.gov/pubmed/23667531');
print "PB url PA - expected: exist but reversed (1) actual: ", isDupPPI($dbh, '79832', '6657', 'http://www.ncbi.nlm.nih.gov/pubmed/23667531');
print "PA url PB - expected: not exist(fake)(0) actual: ", isDupPPI($dbh, '123', '456', 'test');

$dbh->disconnect(); 


#Sample for getGeneInfo
my ($a, $b, $c, @d) = getGeneInfo("oct4");
print "Gene ID: ", $a;
print "Symbol: ", $b;
print "Full Name: ", $c;
print "Aliases: ", "@d";